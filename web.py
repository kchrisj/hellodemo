from flask import Flask
#from flask_cors import CORS

app = Flask(__name__)

@app.route('/')
@app.route('/hello')
def HelloWorld():
    return "Hello World"

if __name__ == '__main__':
    app.debug = False #True
    app.run(host='0.0.0.0', port=5000, debug=False)
